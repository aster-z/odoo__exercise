# -*- coding: utf-8 -*-

from odoo import models, fields, api

class nt_stock_localtion(models.Model):
    _inherit = 'stock.location'

    user_ids = fields.Many2many('res.users', string='库位管理员')

    @api.multi
    def write(self, vals):
        if vals.get('user_ids'):
            u_ids = [t for t in vals.get('user_ids') if t[0] in [4, 6]]
            if u_ids:
                for location in self:
                    location.child_ids.write({'user_ids': u_ids})
        return super(nt_stock_localtion, self).write(vals)

    @api.model
    def create(self, vals):
        parent_id = vals.get('location_id')
        uu_ids = vals.get('user_ids')
        print(uu_ids)
        if parent_id:
            parent = self.search([("id", '=', parent_id)])
            u_ids = [(4, u.id) for u in parent.user_ids]
            vals.update({'user_ids': uu_ids + u_ids})
        return super(nt_stock_localtion, self).create(vals)





