# -*- coding: utf-8 -*-
{
    'name': "nt_zy_stock",

    'summary': """
        sale模块功能修改""",

    'description': """
        不允许销售人员看到非可销售的产品
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale','stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/groups_rules.xml',
        'views/views.xml',
        # 'views/templates.xml',
    ],
}