# -*- coding: utf-8 -*-
{
    'name': "nt_zy_purchase",
    'description': """
        对采购模块源码copy学习
    """,
    'author': "瞿家贵",
    'website': "http://www.yourcompany.com",
    'category': 'Operations/Purchase',
    'version': '1.3',
    'depends': ['account'],
    'data': [
        'security/purchase_security.xml',
        'security/ir.model.access.csv',
        'views/purchase_views.xml',
        'report/purchase_report_views.xml',
        'report/purchase_reports.xml',
        'data/purchase_data.xml'
    ],
    'installable': True,
    'application': True
}
