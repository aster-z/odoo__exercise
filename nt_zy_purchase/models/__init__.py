# -*- coding: utf-8 -*-

from . import res_partner
from . import account_invoice
from . import purchase
from . import res_company
from . import  product
