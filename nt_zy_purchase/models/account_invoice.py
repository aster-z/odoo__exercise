#!/usr/bin/env python
# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', ondelete='set null', index=True, readonly=True)

    purchase_id = fields.Many2one('purchase.order', related='purchase_line_id.order_id', string='Purchase Order',
                                  store=False, readonly=True, related_sudo=False,
                                  help='Associated Purchase Order. Filled in automatically when a PO is chosen on the vendor bill.')