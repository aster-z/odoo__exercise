# -*- coding: utf-8 -*-
{
    'name': "nt_zy_purchase_mrp",

    'summary': """
        复写的purchase_mrp""",

    'description': """
        This module provides facility to the user to install mrp and purchase modules at a time.
        It is basically used when we want to keep track of production orders generated
        from purchase order.
    """,

    'category': 'hidden',
    'version': '0.1',
    'author': 'yzy',

    'depends': ['mrp', 'purchase_stock'],
    'installable': True,
    'auto_install': True,


}